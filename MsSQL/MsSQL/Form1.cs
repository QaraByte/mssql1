﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MsSQL
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;

        public Form1()
        {
            InitializeComponent();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            string connectionString = "Data Source=FUTURE-ПК;Initial Catalog=EnglishDB;Integrated Security=True";
            sqlConnection = new SqlConnection(connectionString);
            await sqlConnection.OpenAsync();

            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM [EngUser]", sqlConnection);

            try
            {
                sqlReader = await command.ExecuteReaderAsync();
                //command.ExecuteNonQuery();
                //command.ExecuteScalar();
                while (await sqlReader.ReadAsync())
                {
                    listBox1.Items.Add(sqlReader["UserId"].ToString() + " " +
                        sqlReader["UserLogin"].ToString() + " " +
                        sqlReader["Password"].ToString() + " " +
                        sqlReader["Name"].ToString() + " " +
                        sqlReader["SecondName"].ToString() + " " +
                        sqlReader["Otchestvo"].ToString() + " " +
                        sqlReader["Email"].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, ex.Source.ToString());
            }
            finally
            {
                if (sqlReader != null)
                    sqlReader.Close();
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtLogin.Text) &&
                !String.IsNullOrEmpty(txtPassw.Text) &&
                !String.IsNullOrEmpty(txtName.Text) &&
                !String.IsNullOrEmpty(txtSecond.Text) &&
                !String.IsNullOrEmpty(txtEmail.Text))
            {
                try
                {
                    SqlCommand command = new SqlCommand("INSERT INTO [EngUser] (UserLogin, Password, Name, SecondName, Email) VALUES (@User, @Passw, @Name, @Sec, @Email)", sqlConnection);
                    command.Parameters.AddWithValue("User", txtLogin.Text);
                    command.Parameters.AddWithValue("Passw", txtPassw.Text);
                    command.Parameters.AddWithValue("Name", txtName.Text);
                    command.Parameters.AddWithValue("Sec", txtSecond.Text);
                    command.Parameters.AddWithValue("Email", txtEmail.Text);

                    await command.ExecuteNonQueryAsync();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля!");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed)
            {
                sqlConnection.Close();
            }
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed)
            {
                sqlConnection.Close();
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM [EngUser]", sqlConnection);

            try
            {
                sqlReader = await command.ExecuteReaderAsync();
                //command.ExecuteNonQuery();
                //command.ExecuteScalar();
                while (await sqlReader.ReadAsync())
                {
                    listBox1.Items.Add(sqlReader["UserId"].ToString() + " " +
                        sqlReader["UserLogin"].ToString() + " " +
                        sqlReader["Password"].ToString() + " " +
                        sqlReader["Name"].ToString() + " " +
                        sqlReader["SecondName"].ToString() + " " +
                        sqlReader["Otchestvo"].ToString() + " " +
                        sqlReader["Email"].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, ex.Source.ToString());
            }
            finally
            {
                if (sqlReader != null)
                    sqlReader.Close();
            }
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE [EngUser] SET [UserLogin]=@User, [Password]=@Passw, [Name]=@Name, [SecondName]=@Sec, [Email]=@Email WHERE [UserId]=@Id", sqlConnection);
                command.Parameters.AddWithValue("User", txtLogin2.Text);
                command.Parameters.AddWithValue("Passw", txtPassw2.Text);
                command.Parameters.AddWithValue("Name", txtName2.Text);
                command.Parameters.AddWithValue("Sec", txtSecond2.Text);
                command.Parameters.AddWithValue("Email", txtEmail2.Text);
                command.Parameters.AddWithValue("Id", txtId2.Text);

                command.ExecuteNonQuery();
                MessageBox.Show("Данные изменены!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM [EngUser] WHERE [UserId]=@Id", sqlConnection);
                command.Parameters.AddWithValue("Id", txtId3.Text);
                command.ExecuteNonQuery();

                MessageBox.Show("Строка " + txtId3.Text + " удалена");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
